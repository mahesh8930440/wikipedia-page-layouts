const bestBowlerEconomy= require("../src/server/9-bowler-best-economy.js");

test("best-Bowler-Economy", () => {
  const testSampleDataDeliveries = [
    {
      match_id: 1,
      bowler: "Bumrah",
      is_super_over:0,
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 1,
      bye_runs:0,
      
    },
    {
      match_id: 2,
      bowler: "Siraj",
      is_super_over:1,
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 4,
      bye_runs:0,
    },
    {
      match_id: 3,
      bowler: "Shami",
      is_super_over:0,
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 4,
      bye_runs:0,
    
    },
    {
      match_id: 4,
      bowler: "TS Mills",
      is_super_over:1,
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 6,
      bye_runs:0,
      
    },
    {
      match_id: 5,
      bowler: "Umesh",
      is_super_over:0,
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 1,
      bye_runs:0,
      
    },
    {
      match_id: 6,
      bowler: "Boult",
      is_super_over:0,
      wide_runs: 0,
      noball_runs: 1,
      total_runs: 2,
      bye_runs:0,
      
    },
  ];
  const resultData = ["Siraj","24.00"]
  expect(bestBowlerEconomy(testSampleDataDeliveries)).toEqual(
    resultData
  );
});