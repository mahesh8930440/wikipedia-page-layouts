const extraRunsConcededPerTeam= require("../src/server/3-extra-runs-conceded-per-team-2016");

test("extra run conceded per team", () => {
  const testSampleDataMatches = [
    {
      id: "1",
      season: "2008",
      
    },
    {
      id: "2",
      season: "2016",
      
    },
    {
      id: "3",
      season: "2010",
     
    },
    {
      id: "4",
      season: "2016",
      
    },
    {
      id: "5",
      season: "2010",
      
    },
    {
      id: "6",
      season: "2016",
      
    },
    {
      id: "7",
      season: "2009",
      
    },
    {
      id: "8",
      season: "2016",
      
    },
    
  ];

  const testSampleDataDeliveries = [
    {
      match_id: "1",
      extra_runs: 2,
      bowling_team: "SRH",
    },
    {
      match_id: "2",
      extra_runs: 0,
      bowling_team: "MI",
    },
    {
      match_id: "3",
      extra_runs: 1,
      bowling_team: "MI",
    },
    {
      match_id: "4",
      extra_runs: 3,
      bowling_team: "RR",
    },
    {
      match_id: "5",
      extra_runs: 0,
      bowling_team: "RCB",
    },
    {
      match_id: "6",
      extra_runs: 1,
      bowling_team: "RR",
    },
    {
      match_id: "7",
      extra_runs: 3,
      bowling_team: "KKR",
    },
    {
      match_id: "8",
      extra_runs: 0,
      bowling_team: "RCB",
    },
    
  ];

  const resultData = { "RR": 4,"MI":0,"RCB":0}
  expect(extraRunsConcededPerTeam(testSampleDataDeliveries,testSampleDataMatches)).toEqual(resultData);
});