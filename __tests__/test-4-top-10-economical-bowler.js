const topEconomicalBowler = require("../src/server/4-top-10-economical-bowler");

test("top-10-economy-bowlers-in-2015", () => {
  const testSampleDataMatches =[
  { id: 1, 
  season: "2014" },
  { id: 2, 
  season: "2015" },
  { id: 3, 
  season: "2016" },
  { id: 4, 
  season: "2015" },
  { id: 5, 
  season: "2009" },
  { id: 6, 
  season: "2015" },
  
  ];

  const testSampleDataDeliveries = [
    {
      match_id: 1,
      bowler: "Bumrah",
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 1,
      bye_runs:0,
      
    },
    {
      match_id: 2,
      bowler: "Siraj",
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 4,
      bye_runs:0,
    },
    {
      match_id: 3,
      bowler: "Shami",
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 4,
      bye_runs:0,
    
    },
    {
      match_id: 4,
      bowler: "TS Mills",
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 6,
      bye_runs:0,
      
    },
    {
      match_id: 5,
      bowler: "Umesh",
      wide_runs: 0,
      noball_runs: 0,
      total_runs: 1,
      bye_runs:0,
      
    },
    {
      match_id: 6,
      bowler: "Boult",
      wide_runs: 0,
      noball_runs: 1,
      total_runs: 2,
      bye_runs:0,
      
    },
    
   ];

    const resultData = [ 
    ["Boult","12.00"],
    ["Siraj","24.00"],
    ["TS Mills","36.00"]]
    expect(topEconomicalBowler(testSampleDataDeliveries, testSampleDataMatches)).toEqual(resultData);
  });
      