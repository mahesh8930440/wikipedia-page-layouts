const teamWinTossAndWinMatch= require("../src/server/5-team-won-toss.js");

test("team-win-toss-and-win-match", () => {
 const testSampleData =[
    {
      season: "2008",
      winner: "RCB",
      toss_winner: "RCB",
    },
    {
      season: "2009",
      winner: "RCB",
      toss_winner: "RCB",
    },
    {
      season: "2010",
      winner: "CSK",
      toss_winner: "KKR",
    },
    { season: "2011", 
      winner: "MI", 
      toss_winner: "MI" },
    {
      season: "2011",
      winner: "KKR",
      toss_winner: "KKR",
    },
    {
      season: "2012",
      winner: "RCB",
      toss_winner: "RCB",
    },
    {
      season: "2013",
      winner: "KKR",
      toss_winner: "RCB",
    },
    {
      season: "2013",
      winner: "RCB",
      toss_winner: "RR"
    },
    
  ];
 const resultData = {
    "RCB": 3,
    "MI": 1,
    "KKR": 1
    
  };
  expect(teamWinTossAndWinMatch(testSampleData)).toEqual(
    resultData
  );
});