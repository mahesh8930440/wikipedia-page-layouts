function extraRunsConcededPerTeam(allDeliveries,allMatches){
    const matchId ={};
    const extraRunsPerTeam={};

    for(let index=0; index<allMatches.length; index++){

        if(allMatches[index].season==2016){
            matchId[allMatches[index].id]=allMatches[index].id;
        }
    }

    for(let index=0; index<allDeliveries.length; index++){
        let match=allDeliveries[index];
        let deliveriesId=match.match_id;
        
        if(matchId[deliveriesId]==deliveriesId){
            let bowlingTeam=match.bowling_team;
            let extraRuns=match.extra_runs;
            
            if(!extraRunsPerTeam[bowlingTeam]){
                extraRunsPerTeam[bowlingTeam]=parseInt(extraRuns);
            }

            else{
                extraRunsPerTeam[bowlingTeam]+=parseInt(extraRuns);
            }
        }    
    }
    return extraRunsPerTeam;
}

module.exports=extraRunsConcededPerTeam;


