function  matchesPlayedForYear(allMatches){
    const allMatchesPlayedForYear ={};

    for (let index=0 ; index<allMatches.length ; index++){
        let keySeason = allMatches[index].season;
        
        if (allMatchesPlayedForYear[keySeason]){
            allMatchesPlayedForYear[keySeason] +=1;
        }

        else {
            allMatchesPlayedForYear[keySeason] =1;
        }
        
    }
    return allMatchesPlayedForYear;
}

module.exports=matchesPlayedForYear;







