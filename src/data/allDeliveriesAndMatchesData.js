const fs = require("fs");
const Papa = require("papaparse");


const deliveriesCsvData = fs.readFileSync("src/data/deliveries.csv", "utf-8");
const matchesCsvData = fs.readFileSync("src/data/matches.csv", "utf-8");


const {data:allDeliveries}= Papa.parse(deliveriesCsvData, {
  header: true,  
  skipEmptyLines: true,  
});

const {data}= Papa.parse(matchesCsvData, {
    header: true,  
    skipEmptyLines: true,  
  });

const  allMatches=data;

module.exports={allDeliveries,allMatches};
